# PipeDrive PHP Class v.0.2 #

This is PHP class for PipeDrive. 
It will allow to operate PipeDrive entities (such as Deals, Persons, etc).

## Notes ##
* Please look for PipeDrive API information here: https://developers.pipedrive.com/v1
* $array should be a hash of proper fields (see PD API for list of all fields)

## Currently available actions ##

* get_all() - to list all entities
* get_one ($id) - get details of one  entity with given $id
* add ($array)  - adds new entity.
* upd ($id, $array) - updates an entity
* is_exist($id) - returns true or false whenther entity with given id is exists

Please note, that class returns always an array, even if there is only one element, e.g.

$deal_data = $PD->get_one(1234);
// use $deal_data[0] to get info

## How to setup ##

1. Enter API key in "pd_def.php" 
2. Include pipedrive.php in your program
3. Make an instance and pass required entity as an argument: $Deals = new PD('deals');

## ChangeLog ##
0.2 - added ERROR_HANDLER for custom error handling
0.1 - init release