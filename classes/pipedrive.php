<?php
require __DIR__.'/../vendor/autoload.php';
require "pd_def.php";
Requests::register_autoloader();

// Base PipeDrive class

class PD {
    
    function __construct ($entity) {
        $this->entity = $entity;
        $this->error_handler = false;
    }
    
    // Get all elements <- us wrapper
    function get_all($params = array(), $body = array()) {
        $this->check_set ($params, 'limit', 500);
        
        $res = array(); // Result
        $go = true;     // Continue marker
        $offset = 0;    // Continue offset
        
        while ($go) {
            $body['start'] = $offset;
            
            $req = $this->us($this->entity, "GET", $params, $body);

            // Push to result array
            foreach ($req->data as $arr) {
                array_push($res, $arr);
            }
            
            if ($req->additional_data->pagination->more_items_in_collection) {
                $go = true;
                // $go = false;
                $offset = $req->additional_data->pagination->next_start;
            } else {
                $go = false;
            }
        }
        return $res;   
    }
    
    function get_one($id) {
        return $this->us($this->entity, "GET", array('id' => $id));
    }
    
    function add($data) {
        if (!is_array($data)) {
            $this->raise_error('ERR_ADD', 'Data should be passed as array');
        }
        return $this->us($this->entity, "POST", array(), $data);
    }
    
    function upd($id, $new_data) {
        return $this->us($this->entity, "PUT", array('id' => $id), $new_data);
    }
    
    function del($id) {
        return $this->us($this->entity, "DELETE", array('id' => $id));
    }
    
    function is_exist($id) {
        $this->error_handler = true;
        $ret = $this->get_one ($id);
        if (!$ret->success) {
            if ($ret->status_code === 404) {
                $ret = false;
            } else {
                // var_dump ($ret);
                $this->raise_http_error($ret, 'is_present');
            } 
        } else {
            $ret = true;
        }   
        
        $this->error_handler = false;
        
        return $ret;
    }
    
    // make request
    function us($part, $meth, $params = null, $body = null) {
        $res = false;
        
        $meth = strtolower($meth);
        
        $url = API_URL . "$part/{$params['id']}?";
        unset ($params['id']);
        
        if ($meth === 'get') {
            $url .= http_build_query($params) ."&";
        }
        
        $url .= "api_token=".API_KEY;
        
        
        /*
        if ( $meth === 'get' ) {
            $params['api_token'] = API_KEY;
            $params = http_build_query($params);
            $url = API_URL . "$part?$params";
        } else {
            // use only ID from params
            $url = API_URL . "$part/{$params['id']}?api_token=".API_KEY;
        }
        */
        // echo $url;
    
        switch ($meth) {
            case 'get':
                $res = Requests::get($url);
            break;
            
            case 'post':
                $res = Requests::post($url, array('Accept' => 'application/json'), $body);
                break;
            break;    
            
            case 'put':
                $res = Requests::put($url, array('Accept' => 'application/json'), $body);
            break;
            
            case 'delete':
                $res = Requests::delete($url, array('Accept' => 'application/json'), $body);
            break;
            
            default:
                $this->raise_error('US_102', 'Incorrect request method');
            break;    
        }
        
        if (!$res->success) {
            if (!$this->error_handler) {
                $this->raise_http_error($res, 'US');
            } else {
                return $res;
            }    
        } 
        
        $body = json_decode ($res->body);
        
        // var_dump ($this->error_handler);
        
        // if (!$body->success && !$this->error_handler) {
        //     $this->raise_error('PD_ERR', $body->error.": ".$body->error_info, 'PD Request');
        // } else if (!$body->success && $this->error_handler) {
        //     return false;
        // } else {    
        //     return $body->data;
        // }
        
        if ($body->success) {
            if ($this->error_handler) {
                $ret = $body;
            } else {
                $ret = $body->data; 
            }
        } else {
            if ($this->error_handler) {
                $ret = $body;
            } else {
                $this->raise_error('PD_ERR', $body->error.": ".$body->error_info, 'PD Request'); 
            }
        }
        
        return $ret;
    }
    
    // --------
    
    function check_set($arr, $where, $what) {
        if (!isset($arr[$where]) ) {
            $arr[$where] = $what;
            $res = true;
        } else {
            $res = false;
        }
        
        return $res;
    }
    
    function set_api_key ($api_key) {
        $this->api_key = $api_key;
    }
    
    function user_interact ($status, $message_array) {
        $out = array(
            "result" => $status,
            "respond" => $message_array
        );
        
        die ( json_encode($out) );
    }
    
    function raise_error ($error_code, $error_message, $domain = null) {
        $this->user_interact ("error", array(
                "error_code" => $error_code,
                "error_message" => $error_message,
                "error_domain" => $domain
            ));
    }
    
    function raise_http_error ($http_object, $domain = null) {
        if (!$http_object->success) {
            $this->raise_error($http_object->status_code, $http_object->error_info, $domain);
        } else {
            return false;
        }
    }
}
?>